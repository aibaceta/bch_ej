xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://osb.bancochile.cl/bch/neg/comex/administrarAgendaBeneficiarioOPE/consultarBeneficiarioPorRutRs/mpi";
(:: import schema at "../Specifications/MSG.001563_1.0.ConsultarBeneficiarioPorRutRs.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/BSS.001346_1.0.sp_sel_agenda_beneficiario";
(:: import schema at "../../../../../BSS.BCH.NEG.COMEX/BSS.001346_1.0.sp_sel_agenda_beneficiario/Specifications/BSS.001346_1.0.sp_sel_agenda_beneficiario_sp.xsd" ::)

declare variable $OutputParameters as element() (:: schema-element(ns1:OutputParameters) ::) external;

declare function local:func($OutputParameters as element() (:: schema-element(ns1:OutputParameters) ::)) as element() (:: schema-element(ns2:consultarBeneficiarioPorRutRs) ::) {
    <ns2:consultarBeneficiarioPorRutRs>
        <ns2:codigoRespuesta>{fn:data($OutputParameters/ns1:cdg_error)}</ns2:codigoRespuesta>
        <ns2:glosaRespuesta>{fn:data($OutputParameters/ns1:dsc_error)}</ns2:glosaRespuesta>
        {
          if(exists($OutputParameters/ns1:RowSet[1]/ns1:Row[1]/ns1:Column[1]/text()))then
            <ns2:Respuesta> 
              {
                for $row in $OutputParameters/ns1:RowSet/ns1:Row
                return
                (
                  <ns2:beneficiario>{
                  (if(exists($row/ns1:Column[2]/text()))then
                    <ns2:nombreBeneficiario>{$row/ns1:Column[2]/text()}</ns2:nombreBeneficiario>
                  else()),
                  (if(exists($row/ns1:Column[5]/text()))then
						<ns2:correoBeneficiario>{$row/ns1:Column[5]/text()}</ns2:correoBeneficiario>
					 else()),
					(if(exists($row/ns1:Column[4]/text()))then              
					<ns2:paisBeneficiario>{$row/ns1:Column[4]/text()}</ns2:paisBeneficiario>
				  else()),
				  (if(exists($row/ns1:Column[3]/text()))then              
					<ns2:direccionBeneficiario>{$row/ns1:Column[3]/text()}</ns2:direccionBeneficiario>
				  else()),
				  (if(exists($row/ns1:Column[1]/text()))then              
					<ns2:aliasBeneficiario>{$row/ns1:Column[1]/text()}</ns2:aliasBeneficiario>
				  else()),
				  (if(exists($row/ns1:Column[6]/text()))then              
					<ns2:telefonoBeneficiario>{$row/ns1:Column[6]/text()}</ns2:telefonoBeneficiario>
				  else()),
				  (if(exists($row/ns1:Column[9]/text()))then              
					<ns2:swiftAbaBeneficiario>{$row/ns1:Column[9]/text()}</ns2:swiftAbaBeneficiario>
				  else()),
				  (if(exists($row/ns1:Column[7]/text()))then              
					<ns2:bancoBeneficiario>{$row/ns1:Column[7]/text()}</ns2:bancoBeneficiario>
				  else()),
				  (if(exists($row/ns1:Column[8]/text()))then              
					<ns2:numeroCuentaBeneficiario>{$row/ns1:Column[8]/text()}</ns2:numeroCuentaBeneficiario>
				  else()),
				  (if(exists($row/ns1:Column[10]/text()))then              
					<ns2:IBANBeneficiario>{$row/ns1:Column[10]/text()}</ns2:IBANBeneficiario>
				  else()),
				  (if(exists($row/ns1:Column[11]/text()))then              
					<ns2:bancoIntermediario>{$row/ns1:Column[11]/text()}</ns2:bancoIntermediario>
				  else()),
				  (if(exists($row/ns1:Column[12]/text()))then              
					<ns2:swiftIntermediario>{$row/ns1:Column[12]/text()}</ns2:swiftIntermediario>
				  else()),
				  (if(exists($row/ns1:Column[13]/text()))then              
					<ns2:gastosAlExterior>{$row/ns1:Column[13]/text()}</ns2:gastosAlExterior>
				  else())
                    }</ns2:beneficiario>
                    )
              }  
              
            </ns2:Respuesta>
        else()
        }
    </ns2:consultarBeneficiarioPorRutRs>
};

local:func($OutputParameters)
xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://osb.bancochile.cl/bch/neg/comex/administrarAgendaBeneficiarioOPE/consultarBeneficiarioPorRutRq/mpi";
(:: import schema at "../Specifications/MSG.001563_1.0.ConsultarBeneficiarioPorRutRq.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/BSS.001346_1.0.sp_sel_agenda_beneficiario";
(:: import schema at "../../../../../BSS.BCH.NEG.COMEX/BSS.001346_1.0.sp_sel_agenda_beneficiario/Specifications/BSS.001346_1.0.sp_sel_agenda_beneficiario_sp.xsd" ::)

declare variable $consultarBeneficiarioPorRutRq as element() (:: schema-element(ns1:consultarBeneficiarioPorRutRq) ::) external;

declare function local:func($consultarBeneficiarioPorRutRq as element() (:: schema-element(ns1:consultarBeneficiarioPorRutRq) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:ope_dsc_rut_cliente>{fn:data($consultarBeneficiarioPorRutRq/ns1:rutCliente)}</ns2:ope_dsc_rut_cliente>
    </ns2:InputParameters>
};

local:func($consultarBeneficiarioPorRutRq)
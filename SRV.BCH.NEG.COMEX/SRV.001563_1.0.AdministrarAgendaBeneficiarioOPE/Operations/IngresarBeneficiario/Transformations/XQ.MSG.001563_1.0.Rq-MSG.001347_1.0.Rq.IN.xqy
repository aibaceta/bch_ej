xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://osb.bancochile.cl/bch/neg/comex/administrarAgendaBeneficiarioOPE/ingresarBeneficiarioRq/mpi";
(:: import schema at "../Specifications/MSG.001563_1.0.IngresarBeneficiarioRq.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/BSS.001347_1.0.sp_ins_agenda_beneficiario";
(:: import schema at "../../../../../BSS.BCH.NEG.COMEX/BSS.001347_1.0.sp_ins_agenda_beneficiario/Specifications/BSS.001347_1.0.sp_ins_agenda_beneficiario_sp.xsd" ::)

declare variable $ingresarBeneficiarioRq as element() (:: schema-element(ns1:ingresarBeneficiarioRq) ::) external;

declare function local:func($ingresarBeneficiarioRq as element() (:: schema-element(ns1:ingresarBeneficiarioRq) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:ope_nmb_benef>{fn:data($ingresarBeneficiarioRq/ns1:nombreBeneficiario)}</ns2:ope_nmb_benef>
        {
            if ($ingresarBeneficiarioRq/ns1:correoBeneficiario)
            then <ns2:ope_dsc_email_benef>{fn:data($ingresarBeneficiarioRq/ns1:correoBeneficiario)}</ns2:ope_dsc_email_benef>
            else ()
        }
        
        <ns2:ope_nmb_pais_benef>{fn:data($ingresarBeneficiarioRq/ns1:paisBeneficiario)}</ns2:ope_nmb_pais_benef>
        <ns2:ope_dsc_direccion_benef>{fn:data($ingresarBeneficiarioRq/ns1:direccionBeneficiario)}</ns2:ope_dsc_direccion_benef>
        <ns2:ope_dsc_alias_benef>{fn:data($ingresarBeneficiarioRq/ns1:aliasBeneficiario)}</ns2:ope_dsc_alias_benef>

        {
            if ($ingresarBeneficiarioRq/ns1:telefonoBeneficiario)
            then <ns2:ope_dsc_telefono_benef>{fn:data($ingresarBeneficiarioRq/ns1:telefonoBeneficiario)}</ns2:ope_dsc_telefono_benef>
            else ()
        }

        <ns2:ope_cdg_swift_aba_benef>{fn:data($ingresarBeneficiarioRq/ns1:swiftAbaBeneficiario)}</ns2:ope_cdg_swift_aba_benef>
        {
            if ($ingresarBeneficiarioRq/ns1:bancoBeneficiario)
            then <ns2:ope_nmb_banco_benef>{fn:data($ingresarBeneficiarioRq/ns1:bancoBeneficiario)}</ns2:ope_nmb_banco_benef>
            else ()
        }

        <ns2:ope_nmr_cta_benef>{fn:data($ingresarBeneficiarioRq/ns1:numeroCuentaBeneficiario)}</ns2:ope_nmr_cta_benef>
        <ns2:ope_cdg_iban_benef>{fn:data($ingresarBeneficiarioRq/ns1:IBANBeneficiario)}</ns2:ope_cdg_iban_benef>
        {
            if ($ingresarBeneficiarioRq/ns1:bancoIntermediario)
            then <ns2:ope_nmb_banco_intermediario>{fn:data($ingresarBeneficiarioRq/ns1:bancoIntermediario)}</ns2:ope_nmb_banco_intermediario>
            else ()
        }
        
        <ns2:ope_cdg_swift_intermediario>{fn:data($ingresarBeneficiarioRq/ns1:swiftIntermediario)}</ns2:ope_cdg_swift_intermediario>
        <ns2:ope_dsc_gasto_exterior>{fn:data($ingresarBeneficiarioRq/ns1:gastosAlExterior)}</ns2:ope_dsc_gasto_exterior>
        <ns2:ope_dsc_rut_cliente>{fn:data($ingresarBeneficiarioRq/ns1:rutCliente)}</ns2:ope_dsc_rut_cliente>
    </ns2:InputParameters>
};

local:func($ingresarBeneficiarioRq)
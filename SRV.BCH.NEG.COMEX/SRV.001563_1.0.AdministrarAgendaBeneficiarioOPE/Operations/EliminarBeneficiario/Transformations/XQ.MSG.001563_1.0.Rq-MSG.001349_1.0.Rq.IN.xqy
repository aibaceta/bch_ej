xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://osb.bancochile.cl/bch/neg/comex/administrarAgendaBeneficiarioOPE/eliminarBeneficiarioRq/mpi";
(:: import schema at "../Specifications/MSG.001563_1.0.EliminarBeneficiarioRq.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/BSS.001349_1.0.sp_del_agenda_beneficiario";
(:: import schema at "../../../../../BSS.BCH.NEG.COMEX/BSS.001349_1.0.sp_del_agenda_beneficiario/Specifications/BSS.001349_1.0.sp_del_agenda_beneficiario_sp.xsd" ::)

declare variable $eliminarBeneficiarioRq as element() (:: schema-element(ns1:eliminarBeneficiarioRq) ::) external;

declare function local:func($eliminarBeneficiarioRq as element() (:: schema-element(ns1:eliminarBeneficiarioRq) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:ope_dsc_rut_cliente>{fn:data($eliminarBeneficiarioRq/ns1:rutCliente)}</ns2:ope_dsc_rut_cliente>
        <ns2:ope_nmr_cta_benef>{fn:data($eliminarBeneficiarioRq/ns1:numeroCuentaBeneficiario)}</ns2:ope_nmr_cta_benef>
        <ns2:ope_dsc_alias_benef>{fn:data($eliminarBeneficiarioRq/ns1:aliasBeneficiario)}</ns2:ope_dsc_alias_benef>
    </ns2:InputParameters>
};

local:func($eliminarBeneficiarioRq)
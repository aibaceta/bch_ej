xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://osb.bancochile.cl/bch/neg/comex/administrarAgendaBeneficiarioOPE/actualizarDatosBeneficiarioRs/mpi";
(:: import schema at "../Specifications/MSG.001563_1.0.ActualizarDatosBeneficiarioRs.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/BSS.001348_1.0.sp_upd_agenda_beneficiario";
(:: import schema at "../../../../../BSS.BCH.NEG.COMEX/BSS.001348_1.0.sp_upd_agenda_beneficiario/Specifications/BSS.001348_1.0.sp_upd_agenda_beneficiario_sp.xsd" ::)

declare variable $OutputParameters as element() (:: schema-element(ns1:OutputParameters) ::) external;

declare function local:func($OutputParameters as element() (:: schema-element(ns1:OutputParameters) ::)) as element() (:: schema-element(ns2:actualizarDatosBeneficiarioRs) ::) {
    <ns2:actualizarDatosBeneficiarioRs>
        <ns2:codigoRespuesta>{fn:data($OutputParameters/ns1:cdg_error)}</ns2:codigoRespuesta>
        <ns2:glosaRespuesta>{fn:data($OutputParameters/ns1:dsc_error)}</ns2:glosaRespuesta>
    </ns2:actualizarDatosBeneficiarioRs>
};

local:func($OutputParameters)
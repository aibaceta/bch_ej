<Errores>
    <Error name="GENERIC">
      <Codigo>GENERIC</Codigo>
      <Tipo>Técnico</Tipo>
      <Descripcion>Error Inesperado</Descripcion>
    </Error>
    <Error name="OSB-380">
      <Codigo>OSB-380</Codigo>
      <Tipo>TimeOut</Tipo>
      <Descripcion>Se ha agotado el tiempo de espera para esta solicitud</Descripcion>
    </Error>
    <Error name="OSB-382">
      <Codigo>OSB-382</Codigo>
      <Tipo>Técnico</Tipo>
      <Descripcion>Error en pipeline</Descripcion>
    </Error>
    <Error name="OSB-386">
      <Codigo>OSB-386</Codigo>
      <Tipo>Técnico</Tipo>
      <Descripcion>Error de seguridad del WS</Descripcion>
    </Error>
    <Error name="OSB-394">
      <Codigo>OSB-394</Codigo>
      <Tipo>Técnico</Tipo>
      <Descripcion>Error en el UDDI subsystem</Descripcion>
    </Error>
    <Error name="LEGADO-01996">
      <Codigo>IF-XML-007</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>No se encontro ningun registro</Descripcion>
    </Error>
    <Error name="LEGADO-01">
      <Codigo>FCCERROR-GENERIC</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>No se puede realizar la consulta</Descripcion>
    </Error>
    <Error name="LEGADO-41">
      <Codigo>41</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>No Disponible</Descripcion>
    </Error>
    <Error name="LEGADO-43">
      <Codigo>43</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Time-OUT</Descripcion>
    </Error>       
    <Error name="LEGADO-55">
      <Codigo>55</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Error en largo del parametro</Descripcion>
    </Error>
    <Error name="LEGADO-60">
      <Codigo>60</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Servicio Inexistente</Descripcion>
    </Error>
    <Error name="LEGADO-96">
      <Codigo>IF-XML-007</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>No se encontro ningun registro</Descripcion>
    </Error>
    <Error name="LEGADO-1106">
      <Codigo>IF-XML-007</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>No se encontro ningun registro</Descripcion>
    </Error>
    <Error name="LEGADO-013000000CLT0u062L05396">
      <Codigo>IF-XML-007</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>No se encontro ningun registro</Descripcion>
    </Error>
    <Error name="LEGADO-013000000SD20U056142096">
      <Codigo>FCCERROR-GENERIC</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>No se puede realizar la consulta</Descripcion>
    </Error>
    <Error name="LEGADO-1009">
      <Codigo>FCCERROR-GENERIC</Codigo>
      <Tipo>Técnico</Tipo>
      <Descripcion>No se puede realizar la consulta</Descripcion>
    </Error>    
    <Error name="FLEXCUBE-FCCERROR">
      <Codigo>FCCERROR</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Error en el Core</Descripcion>
    </Error>
    <Error name="FLEXCUBE-FCCGENERIC">
      <Codigo>FCCGENERIC</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Error en el Core</Descripcion>
    </Error>
    <Error name="BD-382500">
      <Codigo>BD-382500</Codigo>
      <Tipo>Técnico</Tipo>
      <Descripcion>Error de conexion a la BD</Descripcion>
    </Error>
    <Error name="BD-380002">
      <Codigo>BD-380002</Codigo>
      <Tipo>Técnico</Tipo>
      <Descripcion>TIMEOUT</Descripcion>
    </Error>
    <Error name="SRM-53">
      <Codigo>53</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Vista no existe</Descripcion>
    </Error>
    <Error name="SRM-54">
      <Codigo>54</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Operacion no existe</Descripcion>
    </Error>
	<Error name="SRM-55">
      <Codigo>55</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Error en formato</Descripcion>
    </Error>
	<Error name="SRM-56">
      <Codigo>56</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Codigo de transaccion no numerica</Descripcion>
    </Error>
	<Error name="SRM-57">
      <Codigo>57</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Codigo de transaccion no existe o no inicializada</Descripcion>
    </Error>
	<Error name="SRM-58">
      <Codigo>58</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Maximo de transacciones alcanzado</Descripcion>
    </Error>
	<Error name="SRM-59">
      <Codigo>59</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Error al leer archivo de transacciones</Descripcion>
    </Error>
	<Error name="SRM-60">
      <Codigo>60</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Error al grabar archivo de transacciones</Descripcion>
    </Error>
	<Error name="SRM-61">
      <Codigo>61</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Error de datos en el host</Descripcion>
    </Error>
	<Error name="SRM-65">
      <Codigo>65</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Operación no autorizada para la vista especificada</Descripcion>
    </Error>
	<Error name="SRM-66">
      <Codigo>66</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Problemas en vista compleja</Descripcion>
    </Error>
	<Error name="SRM-70">
      <Codigo>70</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Debe iniciar clave</Descripcion>
    </Error>
	<Error name="SRM-71">
      <Codigo>71</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Clave incorrecta</Descripcion>
    </Error>
	<Error name="SRM-72">
      <Codigo>72</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Clave expirada</Descripcion>
    </Error>
	<Error name="SRM-73">
      <Codigo>73</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Grupo no existe</Descripcion>
    </Error>
	<Error name="SRM-74">
      <Codigo>74</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Perfil no existe</Descripcion>
    </Error>
	<Error name="SRM-75">
      <Codigo>75</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Usuario inactivo</Descripcion>
    </Error>
	<Error name="SRM-76">
      <Codigo>76</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Supero intentos permitidos para el login</Descripcion>
    </Error>
	<Error name="SRM-80">
      <Codigo>80</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Registro bloqueado</Descripcion>
    </Error>
	<Error name="SRM-81">
      <Codigo>81</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Tabla de locks esta llena</Descripcion>
    </Error>
	<Error name="SRM-82">
      <Codigo>82</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Error en formato de datos</Descripcion>
    </Error>
	<Error name="SRM-83">
      <Codigo>83</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Registro no esta bloqueado</Descripcion>
    </Error>
	<Error name="SRM-88">
      <Codigo>88</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>ABBEND</Descripcion>
    </Error>
	<Error name="SRM-89">
      <Codigo>89</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Registro Duplicado</Descripcion>
    </Error>
	<Error name="SRM-90">
      <Codigo>90</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Archivo sociado a la vista no existe</Descripcion>
    </Error>
	<Error name="SRM-91">
      <Codigo>91</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Registro con clave duplicada</Descripcion>
    </Error>
	<Error name="SRM-92">
      <Codigo>92</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>No hay espacio, archivo bloqueado</Descripcion>
    </Error>
	<Error name="SRM-93">
      <Codigo>93</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Requerimiento invalido</Descripcion>
    </Error>
	<Error name="SRM-94">
      <Codigo>94</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Error al abrir archivo</Descripcion>
    </Error>
	<Error name="SRM-95">
      <Codigo>95</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Largo no valido</Descripcion>
    </Error>
	<Error name="SRM-96">
      <Codigo>96</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Dato solicitado no se encontro</Descripcion>
    </Error>
	<Error name="SRM-97">
      <Codigo>97</Codigo>
      <Tipo>Funcional</Tipo>
      <Descripcion>Archivo utilizado por el servidor</Descripcion>
    </Error>	
    <Error name="ITEF-01">
      <Codigo>ITEF-01</Codigo>
      <Tipo>Tecnico</Tipo>
      <Descripcion>Rechazada. Transaccion no procesada.</Descripcion>
    </Error>
    <Error name="ITEF-02">
      <Codigo>ITEF-02</Codigo>
      <Tipo>Tecnico</Tipo>
      <Descripcion>Incompleta. Sin respuesta. Se desconoce estado de la transaccion.</Descripcion>
    </Error>
</Errores>
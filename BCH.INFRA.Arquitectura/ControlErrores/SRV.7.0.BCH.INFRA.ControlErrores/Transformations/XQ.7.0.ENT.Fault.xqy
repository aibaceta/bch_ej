xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://osb.bancochile.cl/ent/bch/infra/mci/controlErrores";
(:: import schema at "../../Specifications/MSG.BCH.INFRA.ControlErrores.xsd" ::)
declare namespace con="http://www.bea.com/wli/sb/context";
declare namespace conf="http://www.bea.com/wli/sb/stages/transform/config";
declare namespace det="http://osb.bancochile.cl/ent/bch/infra/mci/errorDetails/v/6";

declare variable $datosErrorFault as element() (:: element(*, ns1:datosErrorFault) ::) external;
declare variable $catalogoError as element() external;

declare function local:generarFault($datosErrorFault as element() (:: element(*, ns1:datosErrorFault) ::),
                                    $catalogoError as element()) {
   
   (: Declaracion de variables :)
   (: En el caso que viene un fault, el codigo de error y motivo se toman del fault. Caso contrario, se toman de las variables que deben venir en la entrada:)
    let $faultCode := if(exists($datosErrorFault/ns1:nodoFault) and not( empty($datosErrorFault/ns1:nodoFault)) ) then 
                        data($datosErrorFault/ns1:nodoFault/con:fault/con:errorCode/text()) 
                      else 
                        data($datosErrorFault/ns1:codigoError/text())        
                      
    let $faultExtras := if(fn:matches($faultCode, '382505')) then (: Si se trata de un error de validacion, puedo obtener los detalles:)
                           if(exists($datosErrorFault/ns1:nodoFault/con:fault/con:details)) then (: Verifico existencia de los detalles:)
                                (: Y concateno los detalles al motivo final:)
                                for $var in ($datosErrorFault/ns1:nodoFault/con:fault/con:details)
                                   for $message in ($var/*)
                                     return fn-bea:serialize($message/*[position()]/text())
                                      (: return if(matches(local-name($message), 'message')) then concat(data($message), ' - ') else():)
                                    (: TODO: Falta concatenar el campo del error. Se debe sacar del nombre del tag:)
                           else()
                        else ()
                        
    let $tipoError := if(exists($datosErrorFault/ns1:nodoFault) and not(empty($datosErrorFault/ns1:nodoFault))) then 
                          if(empty($catalogoError/Error[@name=substring($faultCode, 1, 7)])) then 
                          (: En el caso que tengo un fault, pero el codigo no lo encuentro en el catalogo de errores, asumo error tecnico:)
                            $catalogoError/Error[@name='GENERIC']/Tipo/text() 
                            (: De lo contrario, obtengo el tipo de error, que va a ser tecnico o timeout:)
                            else $catalogoError/Error[@name=substring($faultCode, 1, 7)]/Tipo/text()
                      (: Si no viene un fault, es un error de backend. Reviso catalogo backend:)    
                      else 
                        (: Todo error que venga del back end será considerado funcional :)
                        'Funcional'
    
    let $faultReason := if(fn:matches($faultCode, '382505')) then (: Si se trata un error tecnico de validacion, envio todo el mensaje:)
                           concat(data($datosErrorFault/ns1:nodoFault/con:fault/con:reason/text()), ': ', $faultExtras )
                        else 
                        (: Si me llego un fault, quiere decir que fue un error tecnico:)
                        if(exists($datosErrorFault/ns1:nodoFault) and not(empty($datosErrorFault/ns1:nodoFault))) then 
                          if(empty($catalogoError/Error[@name=substring($faultCode, 1, 7)])) then 
                          (: En el caso que me llega un fault y el error no se encuentra en el catalogo backend, envio error generico:)
                            $catalogoError/Error[@name='GENERIC']/Descripcion/text() 
                            (: Caso contrario, envio la descripcion existente en el catalogo:)
                          else 
                          if($tipoError eq 'TimeOut') then 
                            concat($catalogoError/Error[@name=substring($faultCode, 1, 7)]/Descripcion/text(),", Back End: ", data($datosErrorFault/ns1:backEnd))
                          else
                            $catalogoError/Error[@name=substring($faultCode, 1, 7)]/Descripcion/text()
                        else 
                           (: Si viene del back end, No realizo ningun ajuste. Mando el error tal cual me lo proporciona el back end:)
                           $datosErrorFault/ns1:motivoError/text()
                        
    (: Tengo 2 opciones al momento de obtener el location: Si viene el fault, lo obtengo de ahi. De lo contrario, lo obtengo de las variables persentes en datosErrorFault:)                  
    let $location := if(exists($datosErrorFault/ns1:nodoFault) and not(empty($datosErrorFault/ns1:nodoFault))) then 
                      <det:location>
                          {if(exists($datosErrorFault/ns1:nodoFault/con:fault/con:location/con:node)) then <det:node>{ $datosErrorFault/ns1:nodoFault/con:fault/con:location/con:node/text() }</det:node> else()}
                          {if(exists($datosErrorFault/ns1:nodoFault/con:fault/con:location/con:pipeline)) then <det:pipeline>{ $datosErrorFault/ns1:nodoFault/con:fault/con:location/con:pipeline/text() }</det:pipeline> else()}
                          {if(exists($datosErrorFault/ns1:nodoFault/con:fault/con:location/con:stage)) then <det:stage>{$datosErrorFault/ns1:nodoFault/con:fault/con:location/con:stage/text() }</det:stage> else()}
                          {if(exists($datosErrorFault/ns1:nodoFault/con:fault/con:location/con:path)) then <det:path>{$datosErrorFault/ns1:nodoFault/con:fault/con:location/con:path/text() }</det:path> else()}
                      </det:location>
                      else
                      <det:location>
                          {if(exists($datosErrorFault/ns1:ubicacion/ns1:nodo)) then <det:node>{ $datosErrorFault/ns1:ubicacion/ns1:nodo/text() }</det:node> else()}
                          {if(exists($datosErrorFault/ns1:ubicacion/ns1:pipeline)) then <det:pipeline>{ $datosErrorFault/ns1:ubicacion/ns1:pipeline/text() }</det:pipeline> else()}
                          {if(exists($datosErrorFault/ns1:ubicacion/ns1:etapa)) then <det:stage>{$datosErrorFault/ns1:ubicacion/ns1:etapa/text() }</det:stage> else()}
                          {if(exists($datosErrorFault/ns1:ubicacion/ns1:ruta)) then <det:path>{$datosErrorFault/ns1:ubicacion/ns1:ruta/text() }</det:path> else()}
                      </det:location>
    (: Momentaneo. Verificar lista errores. Agregar lso errores que ya trae  :)
    let $error := 
        <det:error>
          <det:level>Fatal</det:level>
          
          <det:code>{ $faultCode }</det:code>
          <det:description>{ $faultReason }</det:description>
          <det:backEnd>{ if($tipoError eq 'Técnico') then 'OSB' else data($datosErrorFault/ns1:backEnd) }</det:backEnd>
        </det:error>                                    
    
    let $listaErrores :=
        if(exists($datosErrorFault/ns1:listaErrores)) then 
              <det:errorList>
                  {$datosErrorFault/ns1:listaErrores/node()}
                  {$error}
              </det:errorList>
        else
              <det:errorList>
                  {$error}
              </det:errorList>
    
    return
    (: Armado del fault SOAP 1.2 :)
    <env:Fault xmlns:env="http://www.w3.org/2003/05/soap-envelope">
      <env:Code>
        <env:Value>env:Sender</env:Value>
      </env:Code>
      <env:Reason>
        <env:Text xml:lang="en-US">{concat($tipoError, ' : ', $location/det:node/text(),' : ', $faultCode,' : ', $faultReason) }</env:Text>
      </env:Reason>
      <env:Detail>
        <det:details xmlns:det="http://osb.bancochile.cl/ent/bch/infra/mci/errorDetails/v/6">
                <det:fault>
			<det:errorCode>{ $tipoError }</det:errorCode>
			<det:reason>{concat($location/det:node/text(),' : ', $faultCode, ' : ',$faultReason)}</det:reason>
			{$location}
                 </det:fault>       
                <det:fechaHoraInicioTrx>{data($datosErrorFault/ns1:fechaInicio/text())}</det:fechaHoraInicioTrx>
                <det:fechaHoraFinTrx>{fn:current-dateTime()}</det:fechaHoraFinTrx>
                <det:idTransaccionNegocio>{data($datosErrorFault/ns1:idTransaccionNegocio/text())}</det:idTransaccionNegocio>
                <det:errorCode>{ $tipoError }</det:errorCode>
                { $listaErrores }
        </det:details>
      </env:Detail>
    </env:Fault>
};

local:generarFault($datosErrorFault, $catalogoError)
xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace men="http://osb.bancochile.cl/common/MensajeRegistroTrazabilidad";
(:: import schema at "../Specifications/ENT.BCH.INFRA.MensajeTrazabilidad.xsd" ::)

declare function local:func($listaMensajes as element() (:: schema-element(men:listaMensajes) ::),
                            $datosMensajeBasico as element() (:: schema-element(men:datosMensajeBasico) ::),
                            $bodyRequest as xs:string,
                            $headerRequest as xs:string,
                            $bodyResponse as xs:string,
                            $headerResponse as xs:string,
                            $nodoProceso as xs:string,
                            $fechaInicioTransaccion as xs:string,
                            $fechaFinTransaccion as xs:string,
                            $operacionEjecutada as xs:string,
                            $estadoTransaccion as xs:string,
                            $codigoError as xs:string,
                            $descripcionError as xs:string,
                            $localizacionError as xs:string,
                            $detalleError as xs:string,
                            $fault as xs:string,
                            $pathServices as xs:string,
                            $idBusinessServiceNuevo as xs:string?)
                            as element(men:listaMensajes)  {
    
        <men:listaMensajes>
            {$listaMensajes/node()}
            <men:registroTrazabilidad>
                <men:internalCode>{ data($datosMensajeBasico/men:internalCode) }</men:internalCode>
                <men:idTransaccionNegocio>{ data($datosMensajeBasico/men:idTransaccionNegocio) }</men:idTransaccionNegocio>
                <men:fechaInicioTransaccion>{if($fechaInicioTransaccion != "") then xs:dateTime($fechaInicioTransaccion) else()}</men:fechaInicioTransaccion>
                <men:fechaFinTransaccion>{ if($fechaFinTransaccion != "") then xs:dateTime($fechaFinTransaccion) else()}</men:fechaFinTransaccion>
                <men:idServicioProxy>{data($datosMensajeBasico/men:idServicioProxy)}</men:idServicioProxy>
                <men:idServicioBusiness>{ $idBusinessServiceNuevo }</men:idServicioBusiness>
                <men:headerRequest>{ $headerRequest }</men:headerRequest>
                <men:bodyRequest>{ $bodyRequest }</men:bodyRequest>
                <men:headerResponse>{ $headerResponse }</men:headerResponse>
                <men:bodyResponse>{ substring(fn-bea:serialize($bodyResponse),1,3800)}</men:bodyResponse>
                <men:ipCliente>{ data($datosMensajeBasico/men:ipCliente) }</men:ipCliente>
                <men:hostCliente>{ data($datosMensajeBasico/men:hostCliente)}</men:hostCliente>
                <men:usuario>{ data($datosMensajeBasico/men:usuario) }</men:usuario>
                <men:operacionEjecutada>{ $operacionEjecutada }</men:operacionEjecutada>
                <men:nodoProceso>{$nodoProceso}</men:nodoProceso>
                <men:estadoTransaccion>{ $estadoTransaccion }</men:estadoTransaccion>
                <men:codigoError>{$codigoError  }</men:codigoError>
                <men:descripcionError>{substring(fn-bea:serialize($descripcionError),1,3800) }</men:descripcionError>
                <men:localizacionError>{ $localizacionError }</men:localizacionError>
                <men:detalleError>{substring(fn-bea:serialize($detalleError),1,3800)}</men:detalleError>
                <men:fault>{substring(fn-bea:serialize($fault),1,3800)}</men:fault>
                <men:pathServices>{ $pathServices }</men:pathServices>
            </men:registroTrazabilidad>
        </men:listaMensajes>
      
};

declare variable $datosMensajeBasico as element() (:: schema-element(men:datosMensajeBasico) ::) external;
declare variable $listaMensajes as element() (:: schema-element(men:listaMensajes) ::) external;
declare variable $operacionEjecutada as xs:string external;
declare variable $nodoProceso as xs:string external;
declare variable $fechaInicioTransaccion as xs:string external;
declare variable $fechaFinTransaccion as xs:string external;
declare variable $estadoTransaccion as xs:string external;
declare variable $bodyRequest as xs:string external;
declare variable $headerRequest as xs:string external;
declare variable $bodyResponse as xs:string external;
declare variable $headerResponse as xs:string external;
declare variable $codigoError as xs:string external;
declare variable $descripcionError as xs:string external;
declare variable $localizacionError as xs:string external;
declare variable $detalleError as xs:string external;
declare variable $fault as xs:string external;
declare variable $pathServices as xs:string external;
declare variable $idBusinessServiceNuevo as xs:string? external;

local:func($listaMensajes, $datosMensajeBasico, $bodyRequest, $headerRequest, $bodyResponse, $headerResponse, $nodoProceso, $fechaInicioTransaccion, $fechaFinTransaccion, $operacionEjecutada, $estadoTransaccion, $codigoError, $descripcionError, $localizacionError, $detalleError, $fault, $pathServices, $idBusinessServiceNuevo)
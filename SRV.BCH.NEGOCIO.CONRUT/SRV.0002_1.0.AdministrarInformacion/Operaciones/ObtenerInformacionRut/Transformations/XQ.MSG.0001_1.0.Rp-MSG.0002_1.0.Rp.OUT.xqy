xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://osb.bancochile.cl/bch/neg/comex/administrarInformacion/ConsultarPorRutRp/mpi";
(:: import schema at "../Specifications/MSG.0002_1.0.ConsultarPorRutRp.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/BSS.0001__1.0.sp_informacion_obtener";
(:: import schema at "../../../../../BSS.BCH.NEGOCIO.CONRUT/Specifications/BSS.0001__1.0.sp_informacion_obtener_sp.xsd" ::)

declare variable $Resp as element() (:: schema-element(ns1:OutputParameters) ::) external;

declare function local:func($Resp as element() (:: schema-element(ns1:OutputParameters) ::)) as element() (:: schema-element(ns2:FABANClienteInformacionObtenerRespParam) ::) {
    <ns2:FABANClienteInformacionObtenerRespParam>
        {
            if ($Resp/ns1:O_CODIGO_OPERACION)
            then <ns2:codigoOperacion>{fn:data($Resp/ns1:O_CODIGO_OPERACION)}</ns2:codigoOperacion>
            else ()
        }
        {
            if ($Resp/ns1:O_DESCRIPCION)
            then <ns2:glosa>{fn:data($Resp/ns1:O_DESCRIPCION)}</ns2:glosa>
            else ()
        }
        <ns2:informacion></ns2:informacion></ns2:FABANClienteInformacionObtenerRespParam>
};

local:func($Resp)

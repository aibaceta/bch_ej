xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://osb.bancochile.cl/bch/neg/comex/AdministrarInformacion/consultarPorRutRq/mpi";
(:: import schema at "../Specifications/MSG.0002_1.0.ConsultarPorRutRq.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/BSS.0001__1.0.sp_informacion_obtener";
(:: import schema at "../../../../../BSS.BCH.NEGOCIO.CONRUT/Specifications/BSS.0001__1.0.sp_informacion_obtener_sp.xsd" ::)

declare variable $Req as element() (:: schema-element(ns1:FABANClienteInformacionObtenerReqParam) ::) external;

declare function local:func($Req as element() (:: schema-element(ns1:FABANClienteInformacionObtenerReqParam) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:I_RUT_CLIENTE>{fn:data($Req/ns1:Rut)}</ns2:I_RUT_CLIENTE></ns2:InputParameters>
};

local:func($Req)
